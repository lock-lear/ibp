function [nb, index] = searchNbClass(Z, h)
% Cherche combien de feature partage l'histoire h dans la matrice Z
% Ici la matrice Z est de type N * K

[N, D] = size(Z);

index = find(arrayfun(@(j) sum(h == Z(:, j)), 1:D) == N);
nb = length(index);

