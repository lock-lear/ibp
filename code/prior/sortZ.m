function [Z, C, K] = sortZ(Z)
% Trie la matrice Z, et fourni quelques renseignements
% Ouptuts : - Z : la matrice lof(Z input)
%           - C : la classe de chaque histoire
%           - K : la taille de chaque classe (i.e. length(K) <= length(C))

[N, D] = size(Z);

    % S'il n'y a pas de colonne active, ne rien faire
if D == 0
    C = [];
    K = [];
    return;
end

    % Sinon trier
C = zeros(1, D); % liste des classes d'équivalences
Z = sortrows(Z', -1:-1:-N)'; % les - indiquent tri décroissant

    % A partit d'ici on ne fait que du comptage
count = 1;
C(1) = 1;
K = [];
for j = 2:D
    if sum(Z(:, j-1) == Z(:, j)) ~= N
        K = [K sum(C == count)];
        count = count + 1;
    end
    C(j) = count;
end

K = [K sum(C == count)];

end

