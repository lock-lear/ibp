function [Z, K_clean] = cleanZ( Z )
% Cette fonction nettoie la matrice Z de ses colonnes vides, et la renvoie
% accompagnée du nombre effectif de features actives.

    Z(:, ~sum(Z) ) = [];
    K_clean = size(Z, 2);
end
   
