function Z = priorIBPeq(N, alpha, niter,alea)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

Z = zeros(N, 0);
timeiter = 0;

if nargin<4
    alea=0;
end

if alea
    genere_ind = @(N) randperm(N);
else 
    genere_ind = @(N) (1:N);
end


for it = 1:niter
    %disp(strcat('D�but it�ration ', num2str(it), 'temps it�ration pr�c�dente : ', num2str(timeiter)));
    %tic
    %
    ind = genere_ind(N);

    for i = 1:N
        
        Z = cleanZ(Z);
        [Z, Cl, Kcount] = sortZ(Z); % Tr�s gourmant, sera fait via mise � jour
        for h=randperm(length(Kcount))
            
            indicescl = find(Cl == h); % Toutes les colonnes ayant l'histoire h
            workingIndex = indicescl(1); % Une colonne ayant l'histoire h
            m_h = sum(Z(:, workingIndex));
            K_h = searchNbClass(Z, Z(:, workingIndex)); % je le � chaque fois car le mise � jour ne se fait pas en ligne
            pb = 0; % probabilit� de tirer 1
            if Z(i, workingIndex) == 0
                % Si la feature sur la ligne i est inactive
                
                hnew = Z(:, workingIndex);
                hnew(ind(i)) = 1; % La m�me histoire � un d�tail pr�s
                pb = (K_h / (searchNbClass(Z, hnew) + 1)) * ( m_h / max(1, (N - m_h)) );
                
                pb = pb / (1 + pb);
                
            else
                % Si la feature sur la ligne i est d�j� active, la proba est
                % l�g�rement diff�rente
                
                hnew = Z(:, workingIndex);
                hnew(ind(i)) = 0; % La m�me histoire � un d�tail pr�s
                pb = (K_h / (searchNbClass(Z, hnew) + 1)) * ( (N + 1 - m_h) / max(1, (m_h - 1)) );
                
                pb =  1 - pb / (1 + pb);
            end
            %keyboard
            % Ensuite on fait l'exp�rience pour chaque membre de la classe
            % d'�quivalence
            
            dices = binornd(1, pb, 1, length(indicescl));
            Z(ind(i), indicescl) = 0; % On remet tout � plat
            Z(ind(i), indicescl(dices == 1)) = 1; % sauf ceux concern�s
        end
        
        % Et maintenant on regarde si l'on doit rajouter des features
        ei = zeros(N, 1); %
        ei(ind(i)) = 1; % une histoire avec des z�ros partout sauf en i;
        
        offset = searchNbClass(Z, ei);
        tableProbaPoisson = poisspdf(offset + (0:4), alpha / N);
        tableProbaPoisson = tableProbaPoisson / sum(tableProbaPoisson);
        nbnew = randsample(0:4, 1, true, tableProbaPoisson);
        if nbnew > 0
            Z = horzcat(Z, repmat(ei, 1, nbnew));
        end  
    end
    
    %timeiter = toc;
end

Z = cleanZ(Z);
Z = sortZ(Z); % Une derni�re fois pour la route

end