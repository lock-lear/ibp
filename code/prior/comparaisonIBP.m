%%
addpath('/Users/celvira/these/etudeIBP/code');

%% INDIAN BUFFET PROCESS "Classic"
% Une r�alisation de la prior classique, avec ou sans al�a sur l'arriv�e
% des clients

alea = 0; % randperm des samples, default is 0, i=1:N always
Z = priorIBP(100, 10, 100,alea);

figure(1)
clf
colormap gray
imagesc(sortrows(Z', -1:-1:-size(Z, 1))')

%%  SUR CLASSES d'EQUIVALENCE
% Une r�alisation de la prior via classe d'�quivalence, avec ou sans al�a sur l'arriv�e
% des clients

alea = 0; % randperm des samples, default is 0, i=1:N always
Z2 = priorIBPeq(100, 10, 40,alea);

figure(2)
clf
colormap gray
imagesc(Z2)

%%
figure(3)
clf
colormap gray
imagesc(sortrows(Z_2', -1:-1:-size(Z_2, 1))')
title('IBP')

figure(4)
clf
colormap gray
imagesc(Z2_2)
title('IBP �tudi�')







%% Calculs hors �tude
%% Test sur l'optimisation de la recherche des classes similaires.
find(arrayfun(@(j) sum(e == ZZ(:, j)), 1:3) == 4)

tic
searchNbClass(Z, Z(:, 1));
t1 = toc;

tic
nb = 0;
for j = 1: size(Z, 2)
    if sum(Z(:, j) == Z(:, 1)) == size(Z, 1)
       nb = nb + 1; 
    end
end
t2 = toc;
disp(t1);
clear t1;
clear t2;

%% Test sort Z
Ze = Z(:, randperm(size(Z, 2)));
tic
sortZ(Ze);
toc
%clear Ze;

%%
tic
priorIBP(50, 10, 50);
toc