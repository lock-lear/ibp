function Z = priorIBP(N, alpha, niter, alea)
% Cette fonction �chantillonne une matrice Z � partir d'un processus de
% type IBP, tel que d�crit dans le papier de Gharhamani
% args :    - N     : nombre de client
%           - alpha : param�tre du mod�le
%           - niter : nombre d'it�ration de l'�chantilloneur
%           - alea  : Si l'on doit ou non ajouter de l'al�a dans la
%           s�l�ction des clients

if nargin<4
    alea=0;
end

% G�n�re les un ordre d'arriv�e des clients (al�a ou d�terministe)
if alea
    genere_ind = @(N) randperm(N);
else
    genere_ind = @(N) (1:N);
end

Z = zeros(N, 0);
K = 0;
for it = 1:niter
    ind = genere_ind(N);
    
    if it == 1
        p_previousDiviseur = @(i) i; 
        tablePoisRnd = poissrnd(alpha ./ (1:N));
        
    else
        p_previousDiviseur = @(i) N;
        tablePoisRnd = poissrnd(alpha / N, [1 N]); % On g�n�re une table par it�ration
    end
    
    for i = 1:N
        
            % on regarde parmis les clients ayant d�j� fait le choix
        p_previous = sum(Z(ind(1:(i-1)),:),1) / p_previousDiviseur(i); % On compte le nombre de plats
        
            % Ici vu que l'on a l'ind�pendance entre les features (plats) on
            % peut se permettre de g�n�rer tout en m�me temps
        %Z(i, :) = binornd(1, p_previous); pas optimis�
        %disp(unifrnd(0, 1, [1 length(p_previous)]))
        Z(ind(i), :) = unifrnd(0, 1, [1 length(p_previous)]) <= p_previous;
         
        Z = cleanZ(Z);
        K = size(Z, 2);
        
            % Pour le nombre de nouveau client, on utilise une loi de
            % Poisson
        %nbNew = poissrnd(alpha / i, 1, 1); pas optimis�
        nbNew = tablePoisRnd(i);
        if nbNew
            Z = horzcat(Z, zeros(N, nbNew));
            Z(ind(i), (K+1):(K+nbNew)) = 1;
%            K = K + nbNew;
        end
    end
end

end

