%% Etude prior IBP classique

%% IBP Simple
% on se contente ici d'�chantillonner deux IBP, le premier dans sa version
% la plus classique et le second en introduisant de l'al�atoire sur l'ordre
% des clients � chaque it�ration. On observe la plupart du temps un
% d�calage entre l'ordre de grandeur du nombre de feature active

alea = 0; % randperm des samples, default is 0, i=1:N always
Z = priorIBPeq(50, 10, 40, 0);
Z2 = priorIBPeq(50, 10, 40, 1);

%%
figure(1)
subplot(1, 2, 1)
colormap gray
imagesc(Z)
title('IBP classe d �quivalence')

subplot(1, 2, 2)
colormap gray
imagesc(Z2)
title('IBP �quivalence avec al�a sur l ordre des clients � chaque it�ration')


figure(2)
clf
hold on
plot(sort(sum(Z, 1), 'descend'), 'b*')
plot(sort(sum(Z2, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features')
legend('IBP �q', 'IBP �q al�a')
hold off


%% G�n�ration des spectres
% Ici on g�n�re, � param�tres fix�s, une centaine de tirages de l'IBP
% classique et de l'IBP avec al�a. On somme ensuite ensuite les matrices
% lof obtenues et en les normalisants

%% A. G�n�ration
N= 30;
Niter = 100;
Z_saveeq = zeros(N, 100); 
Z_save2eq = zeros(N, 100); 
for i =1:Niter
    disp(['It�ration ', num2str(i)]);
    Zbuf = priorIBPeq(N, 5, 40, 0);
    Z_saveeq(:, 1:size(Zbuf, 2)) = Z_saveeq(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
    Zbuf = priorIBPeq(N, 5, 40, 1);
    Z_save2eq(:, 1:size(Zbuf, 2)) = Z_save2eq(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
end

Z_saveeq = Z_saveeq / Niter;
Z_save2eq = Z_save2eq / Niter;
%% B. Variogramme : �changeabilit�
% Ici on trace simple les variogrammes pour chacune des deux exp�riences.
% On y voit tr�s clairement que les deux versions de l'IBP n'ont clairement
% pas le m�me nombre de feature active. Ceci invalide le fait que l'on
% poss�de la propri�t� d'�changeabilit� dans l'IBP
figure(3)
clf

subplot(3, 1, 1)
colormap default
imagesc(Z_saveeq(:, 1:50))
caxis([0 1])
title('IBP classe d equivalence (100 it�rations, alpha=10)')
xlabel('Plats')
ylabel('clients')

subplot(3, 1, 2)
colormap default
imagesc(Z_save2eq(:, 1:50))
caxis([0 1])
title('IBP classe d equivalence avec al�a sur l ordre des clients � chaque it�ration (100 it�rations, alpha=10)')
xlabel('Plats')
ylabel('clients')

subplot(3, 1, 3)
colormap default
imagesc(abs(Z_saveeq(:, 1:50) - Z_save2eq(:, 1:50)))
caxis([0 1])
title('abs(img 1 - img2)')
colorbar
%% C. D�croissante de la popularit�
% On trace ici l'�volution de la d�croissante dela popularit� des features.
% On y voit clairement un comportement diff�rent, sans interpr�tation pour
% l'instant. 

figure(4)
clf
hold on
plot(sort(sum(Z_saveeq, 1) / size(Z_saveeq, 1), 'descend'), 'b*')
plot(sort(sum(Z_save2eq, 1) / size(Z_save2eq, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features (sur 100 it�rations)')
legend('IBP', 'IBP al�a')
xlabel('Plats(tri�s par ordre d�croissant de popularit�)')
ylabel('popularit� relative')
hold off

%% D. Loi du nombre de feature
%% G�n�ration des donn�es

Niter = 50;
clients = 2:2:50;
alpha = 10;
K_matSizeEq  = zeros(length(clients), Niter);
K_matSizeEq2 = zeros(length(clients), Niter);
count = 1;
for c = clients
    tic
    for i =1:Niter
        Zbuf = priorIBPeq(c, alpha, 40, 0);
        K_matSizeEq(count, i) = size(Zbuf, 2);
        
        Zbuf = priorIBPeq(c, alpha, 40, 1);
        K_matSizeEq2(count, i) = size(Zbuf, 2);
    end
    count = count + 1;
    time = toc;
    disp(['client : ' num2str(c) ' Temps : ' num2str(time)]);
end

clear count;
clear time;
%% R�sultats
figure(10)
clf
hold on
plot(1:50, cumsum(alpha * (1 ./ (1:50)) ))
plot(clients, mean(K_matSizeEq, 2)')
plot(clients, mean(K_matSizeEq2, 2)')
title(['�volution du nombre de features' ' alpha=' num2str(alpha) ' nrepet=' num2str(Niter)])
legend('Esp�rence', 'IBP classique', 'IBP classique avec Al�a')
xlabel('Nombre de clients');
ylabel('Moyenne du nombre de feature');
hold off