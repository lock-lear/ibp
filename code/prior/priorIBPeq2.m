function Z = priorIBPeq(N, alpha, niter,alea)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

Z = zeros(N, 0);
timeiter = 0;

if nargin<4
    alea=0;
end

if alea
    genere_ind = @(N) randperm(N);
else
    genere_ind = @(N) (1:N);
end

ind = genere_ind(N);
hist = zeros(1, 0);
nextHist = 1;
for i = 1:N
    
    % 1. Parcourt des histoires
    buf_hist = hist;
    buf_count = nextHist;
    for wHist = 1:(nextHist-1) % working hist
        
        currentIndex = find(hist == wHist);
        if ~isempty(currentIndex)
            mk = sum(Z(:, currentIndex(1)));
            nbSetToOne = binornd(size(currentIndex), mk / i);
            
            Z(ind(i), currentIndex(1:nbSetToOne)) = 1;
            buf_hist(currentIndex(1:nbSetToOne)) = buf_count;
            buf_count = buf_count + 1;
        end
        
    end
    hist = buf_hist;
    nextHist = buf_count;
    
    % 2. Et maintenant on regarde si l'on doit rajouter des features
    
    ei = zeros(N, 1); %
    ei(ind(i)) = 1; % une histoire avec des z�ros partout sauf en i;
    
    nbnew = poissrnd(alpha / i);
    if nbnew > 0
        Z = horzcat(Z, repmat(ei, 1, nbnew));
    end
    hist = [hist repmat(nextHist, 1, nbnew)];
    nextHist = nextHist + 1;
end

if alea
   Z = sortZ(Z); 
end
end