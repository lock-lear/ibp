%% Etude prior IBP classique

%% IBP Simple
% on se contente ici d'�chantillonner deux IBP, le premier dans sa version
% la plus classique et le second en introduisant de l'al�atoire sur l'ordre
% des clients � chaque it�ration. On observe la plupart du temps un
% d�calage entre l'ordre de grandeur du nombre de feature active

alea = 0; % randperm des samples, default is 0, i=1:N always
Z = priorIBP(100, 10, 100, 0);
Z2 = priorIBP(100, 10, 100, 1);

Z = sortrows(Z', -1:-1:-size(Z, 1))';
Z2 = sortrows(Z2', -1:-1:-size(Z2, 1))';

figure(1)
subplot(1, 2, 1)
colormap gray
imagesc(Z)
title('IBP classque')

subplot(1, 2, 2)
colormap gray
imagesc(Z2)
title('IBP avec al�a sur l ordre des clients � chaque it�ration')


figure(2)
clf
hold on
plot(sort(sum(Z, 1), 'descend'), 'b*')
plot(sort(sum(Z2, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features')
legend('IBP', 'IBP al�a')
hold off


%% G�n�ration des spectres
% Ici on g�n�re, � param�tres fix�s, une centaine de tirages de l'IBP
% classique et de l'IBP avec al�a. On somme ensuite ensuite les matrices
% lof obtenues et en les normalisants

%% A. G�n�ration
N= 30;
Niter = 100;
Z_save = zeros(N, 100);
Z_save2 = zeros(N, 100);
K_size = zeros(1, Niter);
K_size2 = zeros(1, Niter);
for i =1:Niter
    disp(['iter ' num2str(i)]);
    Zbuf = priorIBP(N, 5, 100, 0);
    Z_save(:, 1:size(Zbuf, 2)) = Z_save(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
    K_size(1, i) = size(Zbuf, 2);
    Zbuf = priorIBP(N, 5, 100, 1);
    Z_save2(:, 1:size(Zbuf, 2)) = Z_save2(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
    K_size2(1, i) = size(Zbuf, 2);
end

Z_save = Z_save / Niter;
Z_save2 = Z_save2 / Niter;
%% B. Variogramme : �changeabilit�
% Ici on trace simple les variogrammes pour chacune des deux exp�riences.
% On y voit tr�s clairement que les deux versions de l'IBP n'ont clairement
% pas le m�me nombre de feature active. Ceci invalide le fait que l'on
% poss�de la propri�t� d'�changeabilit� dans l'IBP
figure(3)
clf

subplot(3, 1, 1)
colormap default
imagesc(Z_save(:, 1:50))
title('IBP classique (100 it�rations, alpha=10)')
caxis([0 1])
%colorbar

subplot(3, 1, 2)
colormap default
imagesc(Z_save2(:, 1:50))
caxis([0 1])
title('IBP avec al�a sur l ordre des clients � chaque it�ration (100 it�rations, alpha=10)')
%colorbar;


subplot(3, 1, 3)
colormap default
imagesc(abs(Z_save(:, 1:50) - Z_save2(:, 1:50)))
caxis([0 1])
title('abs(img 1 - img2)')
colorbar
%% C. D�croissante de la popularit�
% On trace ici l'�volution de la d�croissante dela popularit� des features.
% On y voit clairement un comportement diff�rent, sans interpr�tation pour
% l'instant.

figure(4)
clf
hold on
plot(sort(sum(Z_save, 1), 'descend'), 'b*')
plot(sort(sum(Z_save2, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features (sur 100 it�rations)')
legend('IBP', 'IBP al�a')
hold off

%% A ne faire que lorsque IBP et IBP eq sont g�n�r�s

figure(24)
clf
hold on

plot(sort(100*sum(Z_save, 1) / size(Z_save, 1), 'descend'), 'b*')
plot(sort(100*sum(Z_save2, 1) / size(Z_save2, 1), 'descend'), 'bs')

plot(sort(100*sum(Z_saveeq, 1) / size(Z_saveeq, 1), 'descend'), 'r*')
plot(sort(100*sum(Z_save2eq, 1) / size(Z_save2eq, 1), 'descend'), 'rs')

title('D�croissante de la popularit� des features (sur 100 it�rations)')
legend('IBP', 'IBP al�a', 'IBP eq', 'IBP al�a')
xlabel('Feature');
ylabel('Popularit� (en %)');
hold off

%% D. Loi du nombre de feature
%

disp('Nombre moyen de feature IBP classique');
disp(mean(K_size));

disp('Nombre moyen de feature IBP classique avec al�a');
disp(mean(K_size2));

disp('Esp�rence du nombre de feature')
disp(10 * sum(1 ./ (1:100)))

%% G�n�ration des donn�es

Niter = 100;
clients = 10:10:200;
alpha = 10;
K_matSize  = zeros(length(clients), Niter);
K_matSize2 = zeros(length(clients), Niter);
count = 1;
for c = clients
    disp(['client : ' num2str(c)]);
    for i =1:Niter
        Zbuf = priorIBP(c, alpha, 100, 0);
        K_matSize(count, i) = size(Zbuf, 2);
        
        Zbuf = priorIBP(c, alpha, 100, 1);
        K_matSize2(count, i) = size(Zbuf, 2);
    end
    count = count + 1;
end

clear Zbuf;
clear count;
%% R�sultats
figure(10)
clf
hold on
plot(1:200, cumsum(alpha * (1 ./ (1:200)) ))
plot(clients, mean(K_matSize(:, :), 2)')
plot(clients, mean(K_matSize2(:, :), 2)')
title('�volution du nombre de features (alpha=10, 100 it�rations par IBP, 100 r�p�titions)')
legend('Esp�rence', 'IBP classique', 'IBP classique avec Al�a')
xlabel('nombre de clients')
ylabel('nombre moyen de feature')
hold off

%% Enregistrement de matrice 