%% Etude prior IBP classique

%% IBP Simple
% on se contente ici d'�chantillonner deux IBP, le premier dans sa version
% la plus classique et le second en introduisant de l'al�atoire sur l'ordre
% des clients � chaque it�ration. On observe la plupart du temps un
% d�calage entre l'ordre de grandeur du nombre de feature active

alea = 0; % randperm des samples, default is 0, i=1:N always
Z = priorIBP2(100, 10, 10, 0);
Z2 = priorIBP2(100, 10, 10, 1);

Z = sortrows(Z', -1:-1:-size(Z, 1))';
Z2 = sortrows(Z2', -1:-1:-size(Z2, 1))';

figure(1)
subplot(1, 2, 1)
colormap gray
imagesc(Z)
title('IBP classque')

subplot(1, 2, 2)
colormap gray
imagesc(Z2)
title('IBP avec al�a sur l ordre des clients � chaque it�ration')


figure(2)
clf
hold on
plot(sort(sum(Z, 1), 'descend'), 'b*')
plot(sort(sum(Z2, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features')
legend('IBP', 'IBP al�a')
hold off


%% G�n�ration des spectres
% Ici on g�n�re, � param�tres fix�s, une centaine de tirages de l'IBP
% classique et de l'IBP avec al�a. On somme ensuite ensuite les matrices
% lof obtenues et en les normalisants

%% A. G�n�ration
N= 30;
Niter = 100;
Z_saveIbp2 = zeros(N, 100);
Z_saveIbp22 = zeros(N, 100);
K_sizeGibbs = zeros(1, Niter);
K_sizeGibbs2 = zeros(1, Niter);
for i =1:Niter
    disp(['iter ' num2str(i)]);
    Zbuf = priorIBP2(N, 5, 10, 0);
    Z_saveIbp2(:, 1:size(Zbuf, 2)) = Z_saveIbp2(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
    K_sizeGibbs(1, i) = size(Zbuf, 2);
    Zbuf = priorIBP2(N, 5, 10, 1);
    Z_saveIbp22(:, 1:size(Zbuf, 2)) = Z_saveIbp22(:, 1:size(Zbuf, 2)) + sortrows(Zbuf', -1:-1:-size(Zbuf, 1))';
    K_sizeGibbs2(1, i) = size(Zbuf, 2);
end

Z_saveIbp2 = Z_saveIbp2 / Niter;
Z_saveIbp22 = Z_saveIbp22 / Niter;
%% B. Variogramme : �changeabilit�
% Ici on trace simple les variogrammes pour chacune des deux exp�riences.
% On y voit tr�s clairement que les deux versions de l'IBP n'ont clairement
% pas le m�me nombre de feature active. Ceci invalide le fait que l'on
% poss�de la propri�t� d'�changeabilit� dans l'IBP
figure(3)
clf

subplot(3, 1, 1)
colormap default
imagesc(Z_saveIbp2(:, 1:50))
title('IBP classique (100 it�rations, alpha=10)')

subplot(3, 1, 2)
colormap default
imagesc(Z_saveIbp22(:, 1:50))
title('IBP avec al�a sur l ordre des clients � chaque it�ration (100 it�rations, alpha=10)')

subplot(3, 1, 3)
colormap default
imagesc(abs(Z_saveIbp2(:, 1:50) - Z_saveIbp22(:, 1:50)))
caxis([0 1])
title('abs(img 1 - img2)')
colorbar
%% C. D�croissante de la popularit�
% On trace ici l'�volution de la d�croissante dela popularit� des features.
% On y voit clairement un comportement diff�rent, sans interpr�tation pour
% l'instant.

figure(4)
clf
hold on
plot(sort(sum(Z_saveIbp2, 1), 'descend'), 'b*')
plot(sort(sum(Z_saveIbp22, 1), 'descend'), 'r*')
title('D�croissante de la popularit� des features (sur 100 it�rations)')
legend('IBP', 'IBP al�a')
hold off

%% A ne faire que lorsque IBP et IBP eq sont g�n�r�s

figure(24)
clf
hold on

plot(sort(100*sum(Z_saveIbp2, 1) / size(Z_saveIbp2, 1), 'descend'), 'b*')
plot(sort(100*sum(Z_saveIbp22, 1) / size(Z_saveIbp22, 1), 'descend'), 'bs')

plot(sort(100*sum(Z_saveeq, 1) / size(Z_saveeq, 1), 'descend'), 'r*')
plot(sort(100*sum(Z_save2eq, 1) / size(Z_save2eq, 1), 'descend'), 'rs')

title('D�croissante de la popularit� des features (sur 100 it�rations)')
legend('IBP', 'IBP al�a', 'IBP eq', 'IBP al�a')
xlabel('Feature');
ylabel('Popularit� (en %)');
hold off

%% D. Loi du nombre de feature
%

disp('Nombre moyen de feature IBP classique');
disp(mean(K_sizeGibbs));

disp('Nombre moyen de feature IBP classique avec al�a');
disp(mean(K_sizeGibbs2));

disp('Esp�rence du nombre de feature')
disp(10 * sum(1 ./ (1:100)))

%% G�n�ration des donn�es

Niter = 100;
clients = 10:10:200;
alpha = 10;
K_matGibbsSize  = zeros(length(clients), Niter);
K_matGibbs2Size2 = zeros(length(clients), Niter);
count = 1;
for c = clients
    disp(['client : ' num2str(c)]);
    for i =1:Niter
        Zbuf = priorIBP2(c, alpha, 1, 0);
        K_matGibbsSize(count, i) = size(Zbuf, 2);
        
        Zbuf = priorIBP2(c, alpha, 1, 1);
        K_matGibbs2Size2(count, i) = size(Zbuf, 2);
    end
    count = count + 1;
end

clear Zbuf;
clear count;
%% R�sultats
figure(10)
clf
hold on
plot(1:200, cumsum(alpha * (1 ./ (1:200)) ))
plot(clients, mean(K_matGibbsSize(:, :), 2)')
plot(clients, mean(K_matGibbs2Size2(:, :), 2)')
title('�volution du nombre de features')
legend('Esp�rence', 'IBP classique', 'IBP classique avec Al�a')
hold off

%% Enregistrement de matrice 