/*==========================================================
 * arrayProduct.c - example in MATLAB External Interfaces
 *
 * Multiplies an input scalar (multiplier) 
 * times a 1xN matrix (inMatrix)
 * and outputs a 1xN matrix (outMatrix)
 *
 * The calling syntax is:
 *
 *		outMatrix = arrayProduct(multiplier, inMatrix)
 *
 * This is a MEX-file for MATLAB.
 * Copyright 2007-2012 The MathWorks, Inc.
 *
 *========================================================*/

#include "mex.h"

#define INDICES(K, N0, N1)  (N0 + K * N1) // column major
#define KMAX 100


/* Some usefull data */
mxArray *randPerm(mwSize N, int alea) {
    
    if (alea == 1) {
        mxArray *outputRandPerm[1];
        mxArray *input = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
        int *bufInput = (int *)mxGetPr(input);
        bufInput[0] = N;
        mxArray *inputRandPerm[1] = {input};
        int a = mexCallMATLAB(1, outputRandPerm, 1, inputRandPerm, "randperm");
        
        return outputRandPerm[0];
    }
    else {

        mxArray *output = mxCreateNumericMatrix(1, N, mxINT32_CLASS, mxREAL);
        int *ptr = (int *)mxGetPr(output);
        for (int i = 0; i < N; i++)
            ptr[i] = i;
        
        return output;
    }
}

int nextEmpty = 0;
int findNextEmptyColumn(const int *sumColumn) {
    
    if (nextEmpty < KMAX) {
        
        int result = nextEmpty;
        nextEmpty = KMAX;
        for(int k = result + 1; k < KMAX; k++) {
            if (sumColumn[k] == 0) {
                nextEmpty = k;
                break;
            }
        }
        
        return result;
    }
    else
        return -1; // error, no more index (or code issue :))
}

/* La vrai fonction */
mxArray *ibp(int N, double alpha, int niter, int alea) {
    // Fonctionnement du bazar :
    // ici, vu qu'on a besoin que de ce qu'il y a sur la colonne pour calculer 
    // les probas, on va cr�er un tableau de pointeurs. avantage : on parcourt
    // vite les lignes d'une colonnes, ce qu'on fait souvent.
    // Ce qu'il faudrait : un truc du genre une liste, avec push et pop pour 
    // les colonnes. Pour l'instant, un m�ga tableau
    
    int *bufInt;
    double *bufDouble;
    
    //double *listCol[100];
    mxArray *m_Z = mxCreateNumericMatrix(N, KMAX, mxINT32_CLASS, mxREAL);
    int *Z = (int *)mxGetPr(m_Z);
    //for (int n = 0; n < N; n++) for (int k = 0; k < KMAX; k++) Z[INDICES(KMAX, n, k)] = 0;
    // pas besoin d'init en fait dans la construction de mon code
    
    int K = 0;
        // Somme des colonnes
    mxArray *m_sumColum = mxCreateNumericMatrix(1, KMAX, mxINT32_CLASS, mxREAL);
    int *sumColumn = (int *)mxGetPr(m_sumColum);
    
        // Tirage de Bernoulli
    mxArray *m_bernRndOuput[1];
    mxArray *m_bernRndInput[2];
    m_bernRndInput[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
    bufInt = (int *)mxGetPr(m_bernRndInput[0]);
    bufInt[0] = 1;
    
        // Tirage de Poisson
    mxArray *m_PoisRndInput[1];
    mxArray *m_PoisRndOutput[1];
    m_PoisRndInput[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    
    for (int k = 0; k < KMAX; k++) sumColumn[k] = 0; // initialisation
    
    for(int it = 0; it < niter; it++) {
        
        mxArray *m_indices = randPerm(N, alea);
        int *indices = (int *)mxGetPr(m_indices);
        
        for (int n = 0; n < N; n++) {
            
            int curInd = indices[n];
            
                // On d�cr�mente les colomnes concern�es
            for (int k = 0; k < KMAX; k++) {
                if (Z[INDICES(KMAX, curInd, k)] == 1)
                    sumColumn[k]--;
            }
            
                // Je tire KMAX bernoulli, c'est clairement pas optimis�
            m_bernRndInput[1] = m_sumColum;
            if (mexCallMATLAB(1, m_bernRndOuput, 2, m_bernRndInput, "binornd") != 0)
                mexErrMsgIdAndTxt("MyToolbox:ibp:binord","Error while calling binornd");
            bufInt = (int *)mxGetPr(m_bernRndOuput[0]);
            
                // On met tout � jour
            for (int k = 0; k < KMAX; k++) {
                if (bufInt[k] == 1) {
                        // pas besoin d'expliquer
                    Z[INDICES(KMAX, curInd, k)] = 1;
                    sumColumn[k]++;
                }
                else {
                     // nextEmpty est une variable global, qui sert � acc�l�rer la recher d'emplacement vide
                    Z[INDICES(KMAX, curInd, k)] = 0;
                    if (sumColumn[k] == 0 && k < nextEmpty)
                        nextEmpty = k; 
                }
            }
            
                // Maintenant on ajoute des features
            bufDouble = mxGetPr(m_PoisRndInput[0]);
            if (bufDouble == NULL)
                mexPrintf("Was?");

            bufDouble[0] = 0;//alpha / (double)curInd;
            //mexPrintf("%s", bufDouble[0]);
            /*
            if (mexCallMATLAB(1, m_PoisRndOutput, 1, m_PoisRndInput, "poissrnd") != 0)
                mexErrMsgIdAndTxt("MyToolbox:ibp","Error while calling poissrnd");
            bufInt = (int *)mxGetPr(m_PoisRndOutput[0]);
            int nbNew = bufInt[0];
            
            while (nbNew > 0) {
                
                int newFeat = findNextEmptyColumn(sumColumn);
                if (newFeat < 0)
                    mexErrMsgIdAndTxt("MyToolbox:ibp","Too much features, increase KMAX");
                Z[INDICES(KMAX, curInd, newFeat)] = 1;
                sumColumn[newFeat] = 1;
                nbNew--;
            }
             */
        }
    }
    
    //INDICES(K, n, m)
    
    return m_Z;
}

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double multiplier;              /* input scalar */
    double *inMatrix;               /* 1xN input matrix */
    size_t ncols;                   /* size of matrix */
    double *outMatrix;              /* output matrix */

    mwSize alea;
    /****************************************************************/
    /*   V�rifications */
    /****************************************************************/
    
    /* check for proper number of arguments */
    if(nrhs > 4 ) {
        mexErrMsgIdAndTxt("MyToolbox:ibp:nrhs","Pas plus de 4 inputs");
    }
    else if (nrhs < 3)
        mexErrMsgIdAndTxt("MyToolbox:ibp:nrhs","3 arguments minimum");
    
    if(nlhs > 1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","One output required.");
    }
    
    /****************************************************************/
    /*   Analyse input */
    /****************************************************************/
    /* make sure the first input argument is scalar */
    if( /*!mxIsInt32(prhs[0]) ||*/ mxGetNumberOfElements(prhs[0])!= 1 ) {
        mexErrMsgIdAndTxt("MyToolbox:ibp:notInteger","n must be a scalar and integer.");
    }
    
    /* make sure the second input argument is type double */
    if( !mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]) || mxGetNumberOfElements(prhs[1])!= 1) {
        mexErrMsgIdAndTxt("MyToolbox:ibp:notDouble","alpha matrix must be type double and scalar.");
    }
    
    /* check that third argument is scalar and integer */
    if( /*!mxIsInt32(prhs[2]) ||*/ mxGetNumberOfElements(prhs[2])!= 1 ) {
        mexErrMsgIdAndTxt("MyToolbox:ibp:notRowVector","niter must be an integer and scalar.");
    }
    
    /* check that third argument is scalar and integer */
    if (nrhs == 4) {
        if( /*!mxIsInt32(prhs[3]) ||*/ mxGetNumberOfElements(prhs[3])!= 1 ) {
            mexErrMsgIdAndTxt("MyToolbox:ibp:notRowVector","niter must be an integer and scalar.");
        }
        alea = mxGetScalar(prhs[3]);
    }
    else {
        alea = 0;
    }
    
    /* get the value of the scalar input  */
    mwSize N = mxGetScalar(prhs[0]);
    double alpha = mxGetScalar(prhs[1]);
    mwSize niter = mxGetScalar(prhs[2]);

    /* create the output matrix */
    mxArray *Z;

    Z = ibp(N, alpha, niter, alea);
    
    plhs[0] = Z;
}
